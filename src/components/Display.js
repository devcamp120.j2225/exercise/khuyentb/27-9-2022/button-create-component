import { Component } from "react";

class Display extends Component {

    displayComponent = () => {
        this.props.displayProps()
    }

    componentWillMount() {
        console.log('Component Will Mount');
    }

    componentDidMount() {
        console.log('Component Did Mount');
        
    }
   
    componentWillUnmount() {
        console.log("Component will unmount");
    }



    render() {
        return (
            <>
                <button onClick={this.displayComponent}>Destroy Component</button>
                <h1>
                    I Exist !
                </h1>
            </>

        )
    }
}

export default Display;