import { Component } from "react";
import Display from "./Display";

class Parents extends Component {
    constructor(props) {
        super(props);
        this.state = {
            display: false
        }
    }

    handleButtonChangeCreate = () => {
        this.setState({
            display: true
        })
    }

    handleButtonChangeDisplay = () => {
        this.setState({
            display: false
        })
    }
    render() {
        return (
            <>
                {(!this.state.display) ? <button onClick={this.handleButtonChangeCreate}>Create Components</button> : <Display displayProps={this.handleButtonChangeDisplay}></Display>}
            </>


        )
    }
}

export default Parents;